let color_preview = document.getElementById('box-output'),
  r = document.getElementById('r'),
  g = document.getElementById('g'),
  b = document.getElementById('b'),
  hex_out = document.getElementById('hex');

function setColor() {
  let hex = `rgb(${r.value},${g.value},${b.value})`;
  color_preview.style.backgroundColor = hex
  color_preview.innerText = `rgb(${r.value},${g.value},${b.value})`
}