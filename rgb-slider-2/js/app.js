let color_preview = document.querySelector('.color-preview'),
    red = document.getElementById('r'),
    green = document.getElementById('g'),
    blue = document.getElementById('b');

  
function pad(value){
  return (value.length < 2) ? "0" + value : value
}


function setColor(){
  let red_hex = parseInt(red.value,10).toString(16)
  let green_hex = parseInt(green.value,10).toString(16)
  let blue_hex = parseInt(blue.value,10).toString(16)

  let hex_color = "#" + pad(red_hex) + pad(green_hex) + pad(blue_hex) 
  color_preview.style.backgroundColor = hex_color;

  color_preview.innerText = `rgb(${red.value},${green.value}, ${blue.value})`
}


r.addEventListener('change', () => {
  setColor()
},false)


r.addEventListener('input', () => {
  setColor()
},false)




g.addEventListener('change', () => {
  setColor()
},false)


g.addEventListener('input', () => {
  setColor()
},false)




b.addEventListener('change', () => {
  setColor()
},false)


b.addEventListener('input', () => {
  setColor()
},false)